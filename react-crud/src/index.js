import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import GetAll from './GetAll'
import * as serviceWorker from './serviceWorker';
import Get from './Get';
import Create from './Create';
import Update from './Update';
import Delete from './Delete';

class AllCrud extends Component {
    render() { 
        return ( 
            <div>
                <GetAll />
                <Get /> 
                <Create />
                <Update />
                <Delete />
            </div>
         );
    }
}
 
export default AllCrud;

ReactDOM.render(<AllCrud />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
