import React, {Component} from 'react';

import './App.css';

class Get extends Component {

    constructor(props){
        super(props);
        this.state = { car: {} };
    }

    onIdChange = (e) => {
        this.setState({id:e.target.value});    
    }

    getCar(){
        var axios = require('axios');
        axios.get('http://localhost:8080/api/cars/'+this.state.id)
        .then(res=>{
            this.setState({car:res.data[0]});
        })
    }
    
    render() { 
        return ( 
            <div className='get'>
                <h5>See Database for Result</h5>
                <h1>Enter Car Id:</h1>
                <input type='text' onChange = {this.onIdChange} />
                <button onClick = {this.getCar.bind(this)} >Get</button>
                <br/>
                <label>Name: {this.state.car.carname}</label>
                <br/>
                <label>YEAR:  {this.state.car.year}</label> 
            </div>
        );
    }
}
 
export default Get;