import React, {Component} from 'react';
import './App.css';

class Update extends Component {

    constructor(props){
        super(props);
        this.state = { car: {} };
    }

    onIdChange = (e) => {
        this.setState({'id':e.target.value}) 
    }

    onNameChange = (e) => {
        this.setState({'name':e.target.value}) 
    }

    onYearChange = (e) => {
        this.setState({'year':e.target.value})
    }

    updateCar(event){
        var axios = require('axios');
        axios.put('http://localhost:8080/api/cars/'+this.state.id,{
            id:this.state.id,
            carname:this.state.name, 
            year: this.state.year
        })
        .then(res=>{
            console.log(res.data);     
        })
    }
    
    render() { 
        return ( 
            <div className='update'>
                 <h5>See Database for Result</h5>
                <h1>Update</h1>
                <input type='text' placeholder='Enter Car ID' onChange = {this.onIdChange} /> <br /> <br />
                <input type='text' placeholder='Enter Car Name' onChange = {this.onNameChange} /> <br /> <br />
               <input type='text' placeholder='Year' onChange = {this.onYearChange} /><br /> <br />
               <button type='button' onClick = {this.updateCar.bind(this)} > Update </button>
            </div>
        );
    }
}
export default Update;