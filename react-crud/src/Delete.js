import React, {Component} from 'react';

import './App.css';

class Delete extends Component {

    constructor(props){
        super(props);
        this.state = { car: {} };
    }

    onIdChange = (e) => {
        this.setState({id:e.target.value});    
    }

    deleteCar(){
        var axios = require('axios');
        axios.delete('http://localhost:8080/api/cars/'+this.state.id)
        .then(res=>{
            console.log('Deleted Value is: ', res);
        })
    }
    
    render() { 
        return ( 
            <div className='delete'>
                <h5>See Database for Result</h5>
                <h1>Delete Car by Id:</h1>  
                <input type='text' placeholder = 'ID' onChange = {this.onIdChange} />
                <button onClick = {this.deleteCar.bind(this)} >Delete</button>
            </div>
        );
    }
}
 
export default Delete;