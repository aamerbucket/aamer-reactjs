import React, {Component} from 'react';
import './App.css';

class Create extends Component {

    constructor(props){
        super(props);
        this.state = { car: {} };
    }

    onNameChange = (e) => {
        this.setState({'name':e.target.value}) 
    }

    onYearChange = (e) => {
        this.setState({'year':e.target.value})
    }

    saveCar(event){
        var axios = require('axios');
        axios.post('http://localhost:8080/api/cars/',{
            carname:this.state.name, 
            year: this.state.year
        })
        .then(res=>{
            console.log(res.data); 
        })
    }

    render() { 
        return ( 
            <div className='create'>
                 <h5>See Database for Result</h5>
                <h1>Add Car</h1>
               <input type='text' placeholder='Enter Car Name' onChange = {this.onNameChange} /> <br /> <br />
               <input type='text' placeholder='Year' onChange = {this.onYearChange} /><br /> <br />
               <button type='button' onClick = {this.saveCar.bind(this)} > Save </button>
            </div>
        );
    }
}
 
export default Create;