import React, {Component} from 'react';
import './App.css';

class GetAll extends Component {

    constructor(props){
        super(props);
        this.state = {cars:[]};
    }

    componentDidMount(){
        const axios = require('axios');
        axios.get('http://localhost:8080/api/cars/')
        .then(res =>{
            console.log("Response: ", res.data);     
            this.setState({Cars:[res.data]});    
        })
        .catch(error => {
            console.log('Error : ', error);  
        })
    }

    render() { 
        return ( 
            <div className='getAll'>
                <h5>See the console</h5>
                <h1>Cars:</h1>
                <div>
                    
                </div>
                
            </div>
        );
    }
}

export default GetAll;