In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

# Create a component name Greet
1. Create Greet.js
    1. Create a function and return the one of the html tags
    1. Export it 
1. Open App.js
    1. Import Greet.js file
    2. Initialize Greet tag inside of the tags in App.js file
1. Refresh the page

# Multiple components in an single file 

## Declaration
> export const GreetOne = () => <h1> AAMER </h1>
> export const GreetTwo = () => <h1> SOHAIL </h1>

# Export the specific file from muliptiple components
> import { GreetTwo } from './components/Greet.js'
> Inside any one the tags <GreetTwo />

## For JSX functionalities have a look at hellojsx.js file

## Props - props.js - functional components, props_class.js - statfull component

## Passing value from child to parent and then App.js - see ParentComponent.js and ChildComponent.js and App.js

## If else - UserGreetings.js

## List - SimpleList.js

## List - PersonList.js and Person.js