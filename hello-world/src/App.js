import React from 'react';
import logo from './logo.svg';
import './App.css';
import { GreetTwo } from './components/Greet.js'
import Welcome from './components/Welcome.js'
import Hello from './components/hellojsx.js'
import GetName from './components/props.js'
import FetchName from './components/props_class.js'
import OnClickMessage from './components/onclick.js'
import Counter from './components/Counter.js'
import ParentComponent from './components/ParentComponent.js'
import UserGreetings from './components/UserGreetings.js'
import SimpleList from './components/SimpleList.js'
import PersonList from './components/PersonList.js'

function App() {
  return (
    <div className="App">
     {/* <GreetTwo />
     <Welcome />
     <Hello /> */}
     {/* <GetName name = 'Sourodip' surname = 'Chatopadhayay' />
     <GetName name = 'Aamer' surname = 'Chaudhary'> 
      <p> You have created this is program! </p>
     </GetName>
     <GetName name = 'Vaibau' surname = 'Bhatt' /> */}

     {/* below is used for class  */}
     {/* <FetchName /> */}
     {/* <FetchName name= 'KING' surname= 'KHAN'/> */}

    {/* <OnClickMessage/>      */}

    {/* <Counter /> */}

    {/* <ParentComponent /> */}
    
    {/* <UserGreetings /> */}

    {/* <SimpleList /> */}

    <PersonList />

    </div>
  );
}

export default App;
