import React from 'react'

// accept the param
function ChildComponent(props) {
    return (
        <div>
            <button onClick = {() => props.greetHandler(`CHILD`)} >Greet Parent</button>
        </div>
    )
}

export default ChildComponent
