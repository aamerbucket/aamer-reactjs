import React, { Component } from 'react'
import ChildComponent from './ChildComponent.js'

class ParentComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             parentName: 'PARENT'
        }

        // bind the method
        this.greetParent = this.greetParent.bind(this)
    }
    
    greetParent(childName){
        alert(`Hello ${this.state.parentName} from ${childName}` );
    }

    render() {
        return (
            // to access the method from the parent to the child
            // pass the param
            <div>
                <ChildComponent greetHandler = {this.greetParent} />
            </div>
        )
    }
}

export default ParentComponent
