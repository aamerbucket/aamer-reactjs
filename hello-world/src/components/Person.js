import React from 'react'

function Person({n}) {
    return (
        <div>
            <h2> Hi, I am { n.name } and I am { n.age } years old. I know { n.skill } </h2>
        </div>
    )
}

export default Person
