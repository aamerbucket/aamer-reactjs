import React from 'react'

const GetName = props => {
    return (
        // if you have multiple tags, they should go inside the div tag
        <div>
            <h1> Hi... {props.name} {props.surname} </h1>
            {props.children}
        </div>
    )
}

export default GetName