import React from 'react'

const Hello = () => {
    // return (
    //     <div>
    //         <h1> HELLO JSX Example </h1>
    //     </div>
    // )

    // it returns 3 params: A html tag, optional(null), children (for first tag) 
    //return React.createElement('div', 'null', 'Hello JSX'); // but it had created just div tag, if you want to have children tag use React.createElement one more time

    return React.createElement(
        'div', 
        'null', // this param will be used for creating html id, class like attributes 
        React.createElement('h3', null, "Hello JSX"));
}

export default Hello