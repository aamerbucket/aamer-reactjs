import React, { Component } from 'react'

class UserGreetings extends Component {
    
    constructor(props) {
        super(props)
    
        this.state = {
             isLoggedIn: true
        }
    }
    
    render() {
        let message;
        if (this.state.isLoggedIn) {
           message = <div>Hello Aamer</div>
        }
        else{
            message = <div> Sorry, You are not Authorised User </div>
        }
        return message;
    }
}

export default  UserGreetings