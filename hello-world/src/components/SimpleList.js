import React from 'react'

function SimpleList() {
    const names = ['Aamer', 'Sourodip', 'RAMA']
    return (    
        <div>
            {
                names.map( n => <h2> { n } </h2> )
            }
        </div>
    )
}

export default SimpleList
 