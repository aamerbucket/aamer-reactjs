import React, { Component } from 'react'

// type rce
class Counter extends Component {

    // type reconst
    constructor(props) {
        super(props)
    
        this.state = {
           count: 0 
        }
    }
    
    increament(){
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        return (
            <div>
                count {this.state.count}
                <button onClick= { () => this.increament() } >Start</button>
            </div>
        )
    }
}

export default Counter
