import React from 'react'
import Person from './Person';

function PersonList() {
    const persons = [
        {
            id: 1,
            name: 'Aamer',
            age: '27',
            skill: 'AWS'
        },
        {
            id: 2,
            name: 'Sourodip',
            age: '28',
            skill: 'Python'
        },
        {
            id: 3,
            name: 'Rama',
            age: '26',
            skill: '.Net'
        },
        {
            id: 4,
            name: 'Pritam',
            age: '26',
            skill: 'Mech'
        }
    ]
    // this is one way
    //const personNameList = persons.map( n => <h2> Hi, I am { n.name } and I am { n.age } years old. I know { n.skill } </h2> )
    
    // if you want to pass this values to other file example person.js
    const personNameList = persons.map( n => ( <Person key = {n.id} n = { n } /> ) ) // here, key is required for js map()
    return (
                <div> { personNameList } </div>
            )
}

export default PersonList
