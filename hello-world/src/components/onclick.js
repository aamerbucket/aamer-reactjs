import React, { Component } from 'react'

class OnClickMessage extends Component{

    constructor() {
        super()
        this.state = {
            message: 'welcome visitor'
        }
    }

    changeMessage(){
        this.setState ( {
            message: 'Helloo... You are successfully subscribed'
        })
    }

    render(){
        return (
            <div>
                <h1> {this.state.message} </h1>
                <button onClick= { () => this.changeMessage() } >Subscribe</button>
            </div>
        ) 
    }
}

export default OnClickMessage